#ifndef ACTIONS_H_INCLUDED
#define ACTIONS_H_INCLUDED
#include "donnees.h"
//Ce fichier contient les protypes des fonctions de actions.c

void afficherDonneesDansOrdre();
void afficherDonneesTriees();
void rechercheTemps();
void poulsMoyen();
void totalDonnees();
void minimumMaximum();
void moyenneTemps();

#endif //ACTIONS_H_INCLUDED
