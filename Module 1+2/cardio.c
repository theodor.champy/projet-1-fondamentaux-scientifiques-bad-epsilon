#include "cardio.h"
#include "coeur.h"
#include "param.h"

float sensorValue;  // Valeur de A0
unsigned long times[10]={0,0,0,0,0,0,0,0,0,0}; //Tableau des temps de battements
boolean counted=false;  //Définit si un peak a déjà été compté
float bpm;              //battement par minute

float info(){
  sensorValue = analogRead(ANALOG_IN);

  if(sensorValue>295 && counted==false) { //Détecte le peak de la courbe
    for(int i=9;i>0;i--){ //Remplir le tableau du moment des battements
      times[i]=times[i-1];  
    }
    times[0]=millis();
    
         switch(CHOIX){    // Choix est une constante de param.h, on la défini pré-procésseur
          case 1: blinkOnce();    //Toutes les leds clignotent une fois avant un temps mort 
            break;
          case 2: unSurDeux();    //Une led sur 2 s'allume
            break;
          case 3: unSurTrois();   //Une led sur 3 s'allume
            break;
          case 4: chenille();     //Clignotement chenille
            break;
          case 5: doubleBlink();  //Toutes les leds clignotent deux fois avant un temps mort
            break;
          case 6: pimpMyBlink();    //Les leds clignotent d'une façon personnalisée
            break;
          case 7: uneLED(LED);    //Allume une LED au choix
            break;
          default: doubleBlink();
            break;
          }
    float elaps = times[0]-times[9];  //Temps total des 10 battements
    
    bpm=60/((elaps/10)/1000);   //Calcul du bpm
   
    counted=true;   //Evite de recompter le peak
   }
  else if(sensorValue<250){ //On considère la fin d'un peak si la valeur descend sous 250
    counted=false;  
  }
  
  if (times[9]!=0){ //Si le tableau est entièrement rempli
    return bpm;
  }
  else {
    return 0;
  }
}
