extern "C"{
  #include "cardio.h"
}

unsigned long tps;

void setup() {
    pinMode(ANALOG_IN,INPUT);
    Serial.begin(9600);
    delay(1000);
}

void loop() {
  float heartrate;
  int count;
  
  count=info();    
 if (count!=0){
    tps=millis();
    heartrate=(count*60)/(tps/1000);
    if(heartrate<250){
      Serial.print("Bpm = ");
      Serial.print(heartrate);      
      Serial.print("\tTemps = ");
      Serial.println(tps);   
    }                  
  }
}
