#include "generationCode.h"

void genCode(int choix,int nLed){
    FILE *ptrFile;

    ptrFile= fopen("..\\main\\param.h","w"); //Chemin d'accès du fichier
    if (nLed!=0){ // Si le choix de LED est définit (une seule LED à allumer)
        fprintf(ptrFile,"#define CHOIX %d\n#define LED %d",choix,nLed);
    }
    else{ //Tous les autres cas
        fprintf(ptrFile,"#define CHOIX %d\n#define LED -1",choix);
    }
    fclose(ptrFile);    //Fermeture du fichier
}
