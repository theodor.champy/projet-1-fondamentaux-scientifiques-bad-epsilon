#include <stdio.h>
#include <stdlib.h>
#include "menu.h"
#include "generationCode.h"

int main()
{
    int choix;
    choix=menu();   //Récupère le choix du clignotement par l'utilisateur
    if (choix==7){  /* S'il choisit d'allumer une seule led on génère le
                    param.h en précisant la led à allumer avec led() */
       genCode(choix,led());
    }
    else{           //Sinon on génère le param .h normalement
        genCode(choix,0);
    }
    return 0;
}
