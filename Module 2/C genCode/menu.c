#include "menu.h"

int menu(){
    int choix,led;
  do{
        system("CLS");
    printf("Choisissez le mode de clignotement :\n1-Toutes les LEDs s'allument"
           "\n2-1 LED sur 2 est allumee\n3-1 LED sur 3 est allumee\n4-"
           "Clignotement chenille\n5-Double clignotement\n6-Clignotement"
           "loading\n7-Choix de la LED\n0-Quitter\n");
      while(scanf("%d", &choix)==0)//Buffer pour éviter de recevoir une lettre
            {
                printf("Entree invalide, reessayez\n");
                int c;
                while ((c = getchar()) != '\n' && c != EOF) { }
            }
  }while(choix<0 || choix>7); // Le choix doit être compris entre 0 et 7
  return choix;
}

int led(){
    int choix;
       do{
    system("CLS"); //Clearscreen
        printf("Vous avez choisi le choix de LED, quelle LED allumer(3-12)\n");
        while(scanf("%d", &choix)==0)// Même buffer que la fonction précédente
        {
            printf("Entree invalide, reessayez\n");
            int c;
            while ((c = getchar()) != '\n' && c != EOF) { }
        }
       }while(choix<3||choix>12); //Choix entre 3 et 12 soit les ports des LEDs
    return choix;
}
