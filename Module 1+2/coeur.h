#ifndef COEUR_H_INCLUDED
#define COEUR_H_INCLUDED
#include "Arduino.h"

 void chenille();     //Déclaration des prototypes
 void unSurDeux();
 void unSurTrois();
 void doubleBlink();
 void blinkOnce();
 void pimpMyBlink();
 void uneLED(int);
#endif // COEUR_H_INCLUDED
