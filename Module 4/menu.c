#include <stdio.h>
#include <stdlib.h>
#include "menu.h"
//Ce fichier contient les fonctions qui gerent le menu

void afficherMenu()
{
    int menu, restart;
    //Affichage du menu
    printf("1.Affichage des donnees dans l'ordre\n");
    printf("2.Affichage des donnees en ordre croissant/decroissant\n");
    printf("3.Rechercher des donnees pour un temps particulier\n");
    printf("4.Afficher un pouls moyen\n");
    printf("5.Afficher le nombre de lignes de donnees en memoire\n");
    printf("6.Afficher les minimum et maximum de pouls\n");
    printf("7.Afficher la moyenne de pouls sur une periode donnee\n");
    printf("8.Quitter l'application\n");

    //On demande a l'utilisateur ce qu'il veut faire
    //scanf("%d", &menu);
    while(scanf("%d", &menu)==0)//Buffer pour éviter de recevoir une lettre
            {
                printf("Entree invalide, reessayez\n");
                int c;
                while ((c = getchar()) != '\n' && c != EOF) { }
            }
    //On lance differentes fonctions en fonction de l'action demandee
    switch(menu)
    {
        case 1:
            afficherDonneesDansOrdre();
            printf("Relancer le programme ?\n0.Non\n1.Oui\n");
            while(scanf("%d", &restart)==0)//Buffer pour éviter de recevoir une lettre
            {
                printf("Entree invalide, reessayez\n");
                int c;
                while ((c = getchar()) != '\n' && c != EOF) { }
            };
        break;
        case 2:
            afficherDonneesTriees();
            printf("Relancer le programme ?\n0.Non\n1.Oui\n");
            while(scanf("%d", &restart)==0)//Buffer pour éviter de recevoir une lettre
            {
                printf("Entree invalide, reessayez\n");
                int c;
                while ((c = getchar()) != '\n' && c != EOF) { }
            }
        break;
        case 3:
            rechercheTemps();
            printf("Relancer le programme ?\n0.Non\n1.Oui\n");
            while(scanf("%d", &restart)==0)//Buffer pour éviter de recevoir une lettre
            {
                printf("Entree invalide, reessayez\n");
                int c;
                while ((c = getchar()) != '\n' && c != EOF) { }
            }
        break;
        case 4:
            poulsMoyen();
            printf("Relancer le programme ?\n0.Non\n1.Oui\n");
            while(scanf("%d", &restart)==0)//Buffer pour éviter de recevoir une lettre
            {
                printf("Entree invalide, reessayez\n");
                int c;
                while ((c = getchar()) != '\n' && c != EOF) { }
            }
        break;
        case 5:
            totalDonnees();
            printf("Relancer le programme ?\n0.Non\n1.Oui\n");
            while(scanf("%d", &restart)==0)//Buffer pour éviter de recevoir une lettre
            {
                printf("Entree invalide, reessayez\n");
                int c;
                while ((c = getchar()) != '\n' && c != EOF) { }
            }
        break;
        case 6:
            minimumMaximum();
            printf("Relancer le programme ?\n0.Non\n1.Oui\n");
            while(scanf("%d", &restart)==0)//Buffer pour éviter de recevoir une lettre
            {
                printf("Entree invalide, reessayez\n");
                int c;
                while ((c = getchar()) != '\n' && c != EOF) { }
            }
        break;
        case 7:
            moyenneTemps();
            printf("Relancer le programme ?\n0.Non\n1.Oui\n");
            while(scanf("%d", &restart)==0)//Buffer pour éviter de recevoir une lettre
            {
                printf("Entree invalide, reessayez\n");
                int c;
                while ((c = getchar()) != '\n' && c != EOF) { }
            }
        break;
        case 8:
            exit(0);
        default:
            printf("Entrez une valeur valide\n");
            system("CLS");
            afficherMenu();
        break;
    }
    if (restart == 1)
    {
        system("CLS");
        afficherMenu();
    }
    else
    {
        exit(0);
    }
}
