#include "coeur.h"

void chenille(){              //Clignotement chenille
    static x=12;
    
    digitalWrite(x-1,HIGH);
    digitalWrite(x+1,LOW);
    
    if(x==12){
      digitalWrite(x,HIGH);
      digitalWrite(x-1,HIGH);
      digitalWrite(3,LOW);
    }
    if (x==3){
      x=13;
      digitalWrite(x-1,HIGH);
    }
    x--;
}

void unSurDeux(){           //Clignotement d'une LED sur deux
    for(int i=12;i>=3;i--){
      if(((i-2)%2)==0){
        digitalWrite(i,HIGH);
      }
    }
    delay(75);  
    for(int i=12;i>=3;i--){
      if(((i-2)%2)==0){
        digitalWrite(i,LOW);
      }
    }
    delay(75);  
}

void unSurTrois(){          //Clignotement d'une LED sur trois
    for(int i=12;i>=3;i--){
      if(((i-2)%3)==0){
        digitalWrite(i,HIGH);
      }
    }
    delay(75);  
    for(int i=12;i>=3;i--){
      if(((i-2)%3)==0){
        digitalWrite(i,LOW);
      }
    }
    delay(75);
}

void doubleBlink(){     //Clignotement de toutes les LEDs deux fois dans un court laps de temps
   for(int j=0;j<2;j++){
      for(int i=12;i>=3;i--){
        digitalWrite(i,HIGH);   
      }
      delay(40);
      for(int i=12;i>=3;i--){
        digitalWrite(i,LOW);   
      }
      delay(70);
   }
}

void blinkOnce(){             //Clignotement de toutes les LEDs
      for(int i=12;i>=3;i--){
        digitalWrite(i,HIGH);   
      }
      delay(40);
      for(int i=12;i>=3;i--){
        digitalWrite(i,LOW);   
      }
      delay(70);
}
void pimpMyBlink(){         //Les LEDs font une espèce de chargement
  int j=3;
    for(int i=12;i>=7;i--){
        digitalWrite(i,HIGH);   
        digitalWrite(j,HIGH);
          if(j<6){
           j++;
          }
        delay(15);
    }
    delay(30);
    
    for(int i=7;i<=12;i++){
        digitalWrite(i,LOW);   
        digitalWrite(j,LOW);
          if(j>=4){
           j--;
          }
        delay(15);
    }

}

void uneLED(int led){     // Allume une LED définit par l'utilisateur
  digitalWrite(led,HIGH);
  delay(75);
  digitalWrite(led,LOW);
  delay(75);
}
