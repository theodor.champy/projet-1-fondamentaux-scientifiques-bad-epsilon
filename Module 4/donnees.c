#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "donnees.h"
#define BPM_MAX 200
#define HEURE_MAX 24
#define MINUTE_MAX 60
#define SECONDE_MAX 60
//Ce fichier contient les fonctions necessaires à l'obtention et la manipulation des donnees

//Pour tester seulement, a terme cette fonction sera supprimee et on recuperera les donnees a partir du fichier .csv
void genererDonnees()
{
    srand(time(NULL)); //On initialise la fonction aleatoire
    FILE* fichier = NULL;
    fichier = fopen("Battements.csv","w+"); //On ouvre le fichier de donnees

    if (fichier != NULL) //Si l'ouverture du fichier a reussi, on le manipule
    {
        int bpmGenere;
        Time tempsGenere;
        for (int i = 0;i<25;i++)
        {
            //On genere des donnees aleatoires
            bpmGenere = (rand()%BPM_MAX);
            tempsGenere.heure = (rand()%HEURE_MAX);
            tempsGenere.minute = (rand()%MINUTE_MAX);
            tempsGenere.seconde = (rand()%SECONDE_MAX);
            fprintf(fichier, "%d %d %d %d\n", bpmGenere, tempsGenere.heure, tempsGenere.minute, tempsGenere.seconde); //On inscrit les valeurs generees dans le fichier
        }
        fclose(fichier);
    }
    else //Si l'ouverture a echoue, on ferme le programme
    {
        printf("Impossible d'ouvrir le fichier");
        exit(-1);
    }
}

void triBulle(int *tableau, int tailleTableau) //Fonction de tri d'un tableau par tri a bulles
{
    int tmp, tableauTrie;
    do
    {
        tableauTrie = 1;
        for (int i=0;i<=tailleTableau-2;i++)
        {
            if (tableau[i]>tableau[i+1])
            {
                tmp=tableau[i];
                tableau[i]=tableau[i+1];
                tableau[i+1]=tmp;
                tableauTrie=0;
            }
        }
    } while (tableauTrie==0);
}

int quantiteDonnees(FILE *fichier) //Cette fonction sert a obtenir le nombre de donnees presentes dans le fichier
{
    int nombreDonnees = 0;
    while (fgetc(fichier) != EOF) //Cette boucle sert à obtenir le nombre de donnees
        {
            fseek(fichier, -1, SEEK_CUR);
            if (fgetc(fichier) == '\n')
            {
                nombreDonnees++;
                //printf("Il y a %d donnees\n", nombreDonnees);
            }
        }
    return nombreDonnees;
}

void triStructure(Battement *valeursATrier, int tailleTableau) //Fonction de tri a bulle pouvant etre utilisee sur la structure Battement
{
    int tmp, tableauTrie;
    do
    {
        tableauTrie = 1;
        for (int i=0;i<=tailleTableau-2;i++)
        {
            if (valeursATrier[i].bpm>valeursATrier[i+1].bpm)
            {
                tmp=valeursATrier[i].bpm;
                valeursATrier[i].bpm=valeursATrier[i+1].bpm;
                valeursATrier[i+1].bpm=tmp;
                tableauTrie=0;
            }
        }
    } while (tableauTrie==0);
}
