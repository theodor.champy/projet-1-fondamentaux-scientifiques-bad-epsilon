#include "cardio.h"

int sensorValue = 0;
int count=0;
unsigned long time1=0;  // store the initial time
unsigned long time2;    // store the current time
boolean counted=false;

int info(){
  
  if(count==0) {
    time1=millis();
  }
 
  time2=millis();
  sensorValue = analogRead(ANALOG_IN);

  if(sensorValue>290 && counted==false) {
    count++;
    counted=true;
  }
  else if(sensorValue<240){
    counted=false;  
  }
  if (count>0){
   return count;
  }

}
