extern "C"{
  #include "cardio.h"
}

void setup() {
    pinMode(ANALOG_IN,INPUT); //Définit que A0 est une entrée
    Serial.begin(9600);       //Définit le port série pour la lecture
      for(int i=12;i>=3;i--){   // Set les ports 12 à 3 en tant que sortie
        pinMode(i,OUTPUT);      
      }
}

void loop() {
  float bpm;
  int count;
  unsigned long tps; 
  tps=millis(); //Temps depuis le début du programme
  
  bpm=info();    
   if (bpm!=0){ //S'il y a un battement calculé on affiche le bpm et le temps
     if(bpm<250 && bpm>40){ 
      Serial.print(bpm);
      Serial.print(" ");      
      Serial.print(tps);   
      Serial.println(" ");
    }                  
  }

}
