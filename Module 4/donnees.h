#ifndef DONNEES_H_INCLUDED
#define DONNEES_H_INCLUDED
#include <stdlib.h>
#include <stdio.h>
//Ce fichier contient les structures necessaires a la manipulation des donnees

typedef struct Time Time;
struct Time
{
    int heure;
    int minute;
    int seconde;
};

typedef struct Battement Battement;
struct Battement
{
    int bpm;
    Time temps;
};

void genererDonnees();
int fileToStruct(Battement* battements);
void triBulle(int *tableau, int tailleTableau);
int quantiteDonnees(FILE *fichier);

#endif // DONNEES_H_INCLUDED
