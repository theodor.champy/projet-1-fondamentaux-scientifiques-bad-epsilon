#include "actions.h"
#include "donnees.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#define FICHIER_DONNEES "Battements.csv" //Ce define indique le nom du fichier dans lequel lire les donnees
//Ce fichier contient les fonctions correspondant aux differentes actions accessibles depuis le menu

void afficherDonneesDansOrdre()
{
    Battement *battements; //On cree un pointeur pour faire une allocation dynamique
    int nombreDonnees = 0;
    FILE* fichier = NULL;
    fichier = fopen(FICHIER_DONNEES,"r"); //On ouvre le fichier dans la memoire
    if (fichier != NULL) //Si l'ouverture a reussi, on manipule le fichier
    {
        nombreDonnees = quantiteDonnees(fichier); //On recupere le nombre de donnees

        rewind(fichier);
        //printf("Le curseur est en position %ld\n", ftell(fichier));
        battements = malloc(nombreDonnees * sizeof(Battement));
        if (battements == NULL) //Si l'allocation dynamique echoue, on ferme le programme
        {
            printf("Allocation dynamique echouee, merci de racheter des barrettes de ram");
            exit(0);
        }
        double tmp1;
        int tmp2;
        int i = 0;
        while (fgetc(fichier) != EOF && i<nombreDonnees) //On recupere toute les donnees depuis le ficher, on les met dans la structure battements puis on les affiche
        {
            fseek(fichier, -1, SEEK_CUR);
            fscanf(fichier, "%lf %d", &tmp1, &tmp2);
            /*printf("tmp1 = %lf\n", tmp1); //Ce code est utilisé pour tester que la lecture des donnees s'est bien passee
            printf("tmp2 = %d\n", tmp2);*/
            battements[i].bpm=tmp1;
            battements[i].temps.seconde = tmp2/1000;
            battements[i].temps.minute = battements[i].temps.seconde/60;
            battements[i].temps.seconde %= 60;
            battements[i].temps.heure = battements[i].temps.minute/60;
            battements[i].temps.minute %= 60;
            printf("La valeur %d vaut %d\n", i+1, battements[i].bpm);
            //printf("La valeur %d a ete mesuree a %d:%d:%d\n",i+1, battements[i].temps.heure, battements[i].temps.minute, battements[i].temps.seconde);
            //La ligne de code au dessus est utilisee pour tester la lecture du temps
            i++;
        }
        fclose(fichier);
        free(battements); //On ferme le fichier et on libere la memoire
    }
    else // Si on ne peut pas lire le fichier on ferme le programme
    {
        printf("Impossible de lire le fichier");
        exit(-1);
    }
}

void afficherDonneesTriees() //Le code de cette fonction est similaire a celui de la fonction precedente avec un tri a bulle rajoute
{
    Battement *battements;
    FILE* fichier = NULL;
    fichier = fopen(FICHIER_DONNEES,"r");
    if (fichier != NULL)
    {
        int nombreDonnees = quantiteDonnees(fichier);
        rewind(fichier);
        battements = malloc(nombreDonnees * sizeof(Battement));
        if (battements == NULL) //Si l'allocation dynamique echoue, on ferme le programme
        {
            printf("Allocation dynamique echouee, merci de racheter des barrettes de ram");
            exit(0);
        }
        double tmp1;
        int tmp2;
        int i = 0;

        while (fgetc(fichier) != EOF && i<nombreDonnees)
        {
            fseek(fichier, -1, SEEK_CUR);
            fscanf(fichier, "%lf %d", &tmp1, &tmp2);
            /*printf("tmp1 = %lf\n", tmp1); //Ce code est utilisé pour tester que la lecture des donnees s'est bien passee
            printf("tmp2 = %d\n", tmp2);*/
            battements[i].bpm=tmp1;
            battements[i].temps.seconde = tmp2/1000;
            battements[i].temps.minute = battements[i].temps.seconde/60;
            battements[i].temps.seconde %= 60;
            battements[i].temps.heure = battements[i].temps.minute/60;
            battements[i].temps.minute %= 60;
            i++;
        }
        int tmp, tableauTrie; //Tri a bulles applique sur la structure
        do
        {
            tableauTrie = 1;
            for (int i=0;i<=nombreDonnees-2;i++)
            {
                if (battements[i].bpm>battements[i+1].bpm)
                {
                    tmp=battements[i].bpm;
                    battements[i].bpm=battements[i+1].bpm;
                    battements[i+1].bpm=tmp;
                    tableauTrie=0;
                }
            }
        } while (tableauTrie==0);
        for (int j = 0;j<nombreDonnees;j++)
        {
            printf("La valeur %d vaut %d\n", j+1, battements[j].bpm);
        }

        free(battements);
        fclose(fichier);
    }
    else
    {
        printf("Ouverture du fichier impossible");
        exit(0);
    }
}

void rechercheTemps()
{
    Battement *battements;
    FILE* fichier = NULL;
    fichier = fopen(FICHIER_DONNEES,"r");
    if (fichier != NULL)
    {
        int nombreDonnees = quantiteDonnees(fichier);
        rewind(fichier);
        battements = malloc(nombreDonnees * sizeof(Battement));
        if (battements == NULL) //Si l'allocation dynamique echoue, on ferme le programme
        {
            printf("Allocation dynamique echouee, merci de racheter des barrettes de ram");
            exit(0);
        }
        double tmp1;
        int tmp2;
        int i = 0;
        while (fgetc(fichier) != EOF && i<nombreDonnees)
        {
            fseek(fichier, -1, SEEK_CUR);
            fscanf(fichier, "%lf %d", &tmp1, &tmp2);
            /*printf("tmp1 = %lf\n", tmp1); //Ce code est utilisé pour tester que la lecture des donnees s'est bien passee
            printf("tmp2 = %d\n", tmp2);*/
            battements[i].bpm=tmp1;
            battements[i].temps.seconde = tmp2/1000;
            battements[i].temps.minute = battements[i].temps.seconde/60;
            battements[i].temps.seconde %= 60;
            battements[i].temps.heure = battements[i].temps.minute/60;
            battements[i].temps.minute %= 60;
            i++;
        }

        Time tempsRecherche;
        bool trouve = false;
        printf("Entrez le temps au quel a ete effectuee la mesure cherchee\n");
        printf("Heure : ");
        scanf("%d", &tempsRecherche.heure);
        printf("Minutes : ");
        scanf("%d", &tempsRecherche.minute);
        printf("Secondes : ");
        scanf("%d", &tempsRecherche.seconde);

        for (int j=0;j<nombreDonnees;j++) //On parcourt le tableau pour trouver la donnee cherchee
        {
            if (tempsRecherche.heure == battements[j].temps.heure && tempsRecherche.minute == battements[j].temps.minute && tempsRecherche.seconde == battements[j].temps.seconde)
            {
                printf("Le pouls enregistre a %d:%d:%d est %d\n", tempsRecherche.heure, tempsRecherche.minute, tempsRecherche.seconde, battements[j].bpm);
                trouve = true;
            }
        }
        if (trouve == false)
        {
            printf("La mesure cherchee n'a pas ete trouvee");
        }

        free(battements);
        fclose(fichier);
    }
    else
    {
        printf("Impossible de lire le fichier");
        exit(0);
    }
}

void poulsMoyen()
{
    Battement *battements;
    FILE* fichier = NULL;
    fichier = fopen(FICHIER_DONNEES,"r");
    if (fichier != NULL)
    {
        int nombreDonnees = quantiteDonnees(fichier);
        rewind(fichier);
        battements = malloc(nombreDonnees * sizeof(Battement));
        if (battements == NULL) //Si l'allocation dynamique echoue, on ferme le programme
        {
            printf("Allocation dynamique echouee, merci de racheter des barrettes de ram");
            exit(0);
        }
        double tmp1;
        int tmp2;
        int i = 0;
        while (fgetc(fichier) != EOF && i<nombreDonnees)
        {
            fseek(fichier, -1, SEEK_CUR);
            fscanf(fichier, "%lf %d", &tmp1, &tmp2);
            /*printf("tmp1 = %lf\n", tmp1); //Ce code est utilisé pour tester que la lecture des donnees s'est bien passee
            printf("tmp2 = %d\n", tmp2);*/
            battements[i].bpm=tmp1;
            battements[i].temps.seconde = tmp2/1000;
            battements[i].temps.minute = battements[i].temps.seconde/60;
            battements[i].temps.seconde %= 60;
            battements[i].temps.heure = battements[i].temps.minute/60;
            battements[i].temps.minute %= 60;
            i++;
        }

        double moyenne = 0; //On calcule la moyenne a partir des donnees contenues dans la structure
        for (int j=0;j<nombreDonnees;j++)
        {
            moyenne += battements[j].bpm;
        }
        moyenne = moyenne / nombreDonnees;
        printf("Le pouls moyen est de %lf", moyenne);

        free(battements);
        fclose(fichier);
    }
    else
    {
        printf("Impossible de lire le fichier");
        exit(0);
    }
}

void totalDonnees()
{
    FILE* fichier = NULL;
    fichier = fopen(FICHIER_DONNEES,"r");
    if (fichier != NULL)
    {
        printf("Il y a %d lignes de donnees", quantiteDonnees(fichier)); //On utilise juste la fonction quantiteDonnees
        fclose(fichier);
    }
    else
    {
        printf("Impossible de lire le fichier");
        exit(0);
    }
}

void minimumMaximum()
{
    Battement *battements;
    FILE* fichier = NULL;
    fichier = fopen(FICHIER_DONNEES,"r");
    if (fichier != NULL)
    {
        int nombreDonnees = quantiteDonnees(fichier);
        rewind(fichier);
        battements = malloc(nombreDonnees * sizeof(Battement));
        if (battements == NULL) //Si l'allocation dynamique echoue, on ferme le programme
        {
            printf("Allocation dynamique echouee, merci de racheter des barrettes de ram");
            exit(0);
        }
        double tmp1;
        int tmp2;
        int i = 0;
        while (fgetc(fichier) != EOF && i<nombreDonnees)
        {
            fseek(fichier, -1, SEEK_CUR);
            fscanf(fichier, "%lf %d", &tmp1, &tmp2);
            /*printf("tmp1 = %lf\n", tmp1); //Ce code est utilisé pour tester que la lecture des donnees s'est bien passee
            printf("tmp2 = %d\n", tmp2);*/
            battements[i].bpm=tmp1;
            battements[i].temps.seconde = tmp2/1000;
            battements[i].temps.minute = battements[i].temps.seconde/60;
            battements[i].temps.seconde %= 60;
            battements[i].temps.heure = battements[i].temps.minute/60;
            battements[i].temps.minute %= 60;
            i++;
        }

        int maximum = 0, minimum = 200;
        for (int j=0;j<nombreDonnees;j++)
        {
            if (battements[j].bpm < minimum) //On parcourt le tableau pour trouver la valeur la plus petite
            {
                minimum = battements[j].bpm;
            }
        }
        for (int j=0;j<nombreDonnees;j++) //On parcourt le tableau pour trouver la valeur la plus grande
        {
            if (battements[j].bpm > maximum)
            {
                maximum = battements[j].bpm;
            }
        }
        printf("Le pouls maximum est de %d et le pouls minimum est de %d", maximum, minimum);

    free(battements);
    fclose(fichier);
    }
    else
    {
        printf("Impossible de lire le fichier");
        exit(0);
    }
}

void moyenneTemps()
{
    Time heureUn, heureDeux;
    printf("Entrez l'heure de début\nHH MM SS\n");
    scanf("%d %d %d", &heureUn.heure, &heureUn.minute, &heureUn.seconde);
    printf("Entrez l'heure de fin\nHH MM SS\n");
    scanf("%d %d %d", &heureDeux.heure, &heureDeux.minute, &heureDeux.seconde);

    Battement *battements;
    FILE* fichier = NULL;
    fichier = fopen(FICHIER_DONNEES,"r");
    if (fichier != NULL)
    {
        int nombreDonnees = quantiteDonnees(fichier);
        rewind(fichier);
        battements = malloc(nombreDonnees * sizeof(Battement));
        if (battements == NULL) //Si l'allocation dynamique echoue, on ferme le programme
        {
            printf("Allocation dynamique echouee, merci de racheter des barrettes de ram");
            exit(0);
        }
        double tmp1;
        int tmp2;
        int i = 0;
        while (fgetc(fichier) != EOF && i<nombreDonnees)
        {
            fseek(fichier, -1, SEEK_CUR);
            fscanf(fichier, "%lf %d", &tmp1, &tmp2);
            /*printf("tmp1 = %lf\n", tmp1); //Ce code est utilisé pour tester que la lecture des donnees s'est bien passee
            printf("tmp2 = %d\n", tmp2);*/
            battements[i].bpm=tmp1;
            battements[i].temps.seconde = tmp2/1000;
            battements[i].temps.minute = battements[i].temps.seconde/60;
            battements[i].temps.seconde %= 60;
            battements[i].temps.heure = battements[i].temps.minute/60;
            battements[i].temps.minute %= 60;
            i++;
        }

        int indexHeureUn, indexHeureDeux;
        for (int j=0;j<nombreDonnees;j++)
        {
            bool heureTrouvee = false;
            if (heureTrouvee == false && battements[j].temps.seconde == heureUn.seconde && battements[j].temps.minute == heureUn.minute && battements[j].temps.seconde == heureUn.seconde)
            {
                indexHeureUn = j;
                heureTrouvee = true;
            }
        }
        for (int j=0;j<nombreDonnees;j++)
        {
            bool heureTrouvee = false;
            if (heureTrouvee == false && battements[j].temps.seconde == heureDeux.seconde && battements[j].temps.minute == heureDeux.minute && battements[j].temps.seconde == heureDeux.seconde)
            {
                indexHeureDeux = j;
                heureTrouvee = true;
            }
        }

        int moyenne = 0;
        for (int j = indexHeureUn;j<=indexHeureDeux;j++)
        {
            moyenne += battements[j].bpm;
        }
        moyenne = moyenne / nombreDonnees;

    free(battements);
    fclose(fichier);
    }
    else
    {
        printf("Lecture du fichier impossible");
        exit(0);
    }
}
